import api from "../config/api";

const endpoints = {
  async search(searchQuery, page, perPage) {
    return api.get("/search/users", {
      params: {
        q: searchQuery,
        page,
        per_page: perPage,
      },
    });
  },
  async profile(loginName) {
    return api.get(`/users/${loginName}`);
  },
};

export default endpoints;
