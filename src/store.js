import React, { createContext, useReducer } from "react";

const initialState = {
  loadingSearch: false,
  loadingProfile: false,
  searchTermValue: "",
  searchTerm: "",
  searchPage: 1,
  searchPerPage: 12,
  results: [],
  totalItems: 0,
  modalVisible: false,
  loadingUserProfile: false,
  loadingUserId: 0,
  userProfile: null,
};

const store = createContext(initialState);
const { Provider } = store;

const StateProvider = ({ children }) => {
  const [state, dispatch] = useReducer((state, action) => {
    const { type, payload = {} } = action;

    switch (type) {
      case "updateState":
        return { ...state, ...payload };
      default:
        throw new Error("Undefined action");
    }
  }, initialState);

  return <Provider value={{ state, dispatch }}>{children}</Provider>;
};

export { store, StateProvider };
