export const createAction = (type) => (payload) => ({ type, payload });

export const handleApiError = (error) => {
  const {
    status,
    data: { message },
  } = error.response;

  return `Erro ${status} - ${message}`;
};
