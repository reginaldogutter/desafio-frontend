import React from "react";
import { format } from "date-fns";

import "./index.css";

export default function Modal({ onClose, visible, data }) {
  return (
    <div className={`modal-mask ${visible ? "modal-show" : "modal-hide"}`}>
      <div className="modal-body">
        {data !== null && (
          <div className="modal-content">
            <div className="profile-image">
              <img src={data.avatar_url} alt={data.login} />
            </div>
            <div className="profile-info">
              <div className="info-header">
                <h4>{data.login}</h4>
              </div>
              <div className="info-body">
                <div className="info-row">
                  <div className="info-col">
                    <p className="label">Username:</p>
                    <p className="value">{data.login}</p>
                  </div>
                  <div className="info-col">
                    <p className="label">Seguindo:</p>
                    <p className="value">{data.following}</p>
                  </div>
                </div>
                <div className="info-row">
                  <div className="info-col">
                    <p className="label">Cadastrado(a):</p>
                    <p className="value">
                      {format(new Date(data.created_at), "dd-MM-yyyy")}
                    </p>
                  </div>
                  <div className="info-col">
                    <p className="label">Seguidores:</p>
                    <p className="value">{data?.followers}</p>
                  </div>
                </div>
                <div className="info-row">
                  <div className="info-col">
                    <p className="label">URL:</p>
                    <a
                      href={data.html_url}
                      target="_blank"
                      rel="noopener noreferrer"
                      className="value"
                    >
                      {data.html_url}
                    </a>
                  </div>
                </div>
              </div>
              <button className="modal-close-btn" onClick={onClose}>
                Fechar
              </button>
            </div>
          </div>
        )}
        {data === null && (
          <div className="modal-content">
            <p>Nenhum dado encontrado.</p>
          </div>
        )}
      </div>
    </div>
  );
}
