import React from "react";
import "./index.css";

export default function Footer({ copyright }) {
  return (
    <div className="footer">
      <div className="footer-content">
        <p>{copyright}</p>
      </div>
    </div>
  );
}
