import React from "react";

import loadingGif from "../../assets/icons/loading.gif";
import "./index.css";

export default function Card({ data, loading, userId, onSeeDetails }) {
  return (
    <div className="card-container">
      <div className="profile-image-container">
        <img src={data.avatar_url} alt={data.login} />
      </div>
      <div className="profile-info-container">
        <h5>{data.login}</h5>
        <a href={data.html_url} target="_blank" rel="noreferrer noopener">
          {data.html_url}
        </a>
        <p>{`Score: ${data.score.toFixed(1)}`}</p>
      </div>
      <div className="see-more-container">
        <button onClick={onSeeDetails}>
          {loading && data.id === userId ? (
            <img src={loadingGif} width={20} height={20} alt="Carregando" />
          ) : (
            "Ver mais"
          )}
        </button>
      </div>
    </div>
  );
}
