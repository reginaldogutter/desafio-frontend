import React, { useContext } from "react";
import { useHistory } from "react-router-dom";

import { store } from "../../store";
import { useSearchUsers } from "../../models/users";

import logotype from "../../assets/logotype.svg";
import magnifier from "../../assets/icons/magnifier.png";
import loading from "../../assets/icons/loading.gif";

import "./index.css";
import { createAction } from "../../utils/utils";

export default function Header() {
  const history = useHistory();

  const {
    dispatch,
    state: { searchTermValue, loadingSearch },
  } = useContext(store);

  const { execute } = useSearchUsers();

  const doSearch = async () => {
    dispatch(
      createAction("updateState")({
        searchPage: 1,
        searchPerPage: 12,
      })
    );
    await execute(searchTermValue, 1, 12);
    history.push("/results");
  };

  const doNavigateHome = (e) => {
    e.preventDefault();

    if (history.location.pathname !== "/") {
      dispatch(
        createAction("updateState")({
          searchTermValue: "",
          searchTerm: "",
        })
      );

      history.push("/");
    }
  };

  return (
    <div className="header">
      <div className="header-content">
        <a href="/" onClick={(e) => doNavigateHome(e)}>
          <img
            className="header-logo"
            src={logotype}
            alt="The Git Search - Buscar no Github"
            width={78}
            height={36}
          />
        </a>
        <div className="search-input">
          <input
            type="text"
            placeholder="Pesquisar"
            value={searchTermValue}
            onChange={(e) =>
              dispatch(
                createAction("updateState")({ searchTermValue: e.target.value })
              )
            }
          />
          <button type="button" onClick={doSearch}>
            <img
              src={loadingSearch ? loading : magnifier}
              width={20}
              height={20}
              alt="Pesquisar"
            />
          </button>
        </div>
      </div>
    </div>
  );
}
