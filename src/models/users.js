import { useContext, useCallback } from "react";
import { useToasts } from "react-toast-notifications";

import userService from "../services/users";
import { store } from "../store";
import { createAction, handleApiError } from "../utils/utils";

export const useSearchUsers = () => {
  const { dispatch } = useContext(store);
  const { addToast } = useToasts();

  const execute = async (searchTerm, page, perPage) => {
    try {
      dispatch(
        createAction("updateState")({
          loadingSearch: true,
          searchTerm,
          searchPage: page,
        })
      );

      const {
        data: { total_count, items },
      } = await userService.search(searchTerm, page, perPage);

      dispatch(
        createAction("updateState")({
          loadingSearch: false,
          results: items,
          totalItems: total_count,
        })
      );
    } catch (e) {
      addToast(handleApiError(e), {
        appearance: "error",
        autoDismiss: true,
      });

      dispatch(
        createAction("updateState")({
          loadingSearch: false,
        })
      );

      throw e;
    }
  };

  return {
    execute: useCallback(execute, [addToast, dispatch]),
  };
};

export const useUserProfile = () => {
  const { dispatch } = useContext(store);
  const { addToast } = useToasts();

  const execute = async (user) => {
    try {
      dispatch(
        createAction("updateState")({
          loadingUserProfile: true,
          loadingUserId: user.id,
        })
      );

      const { data } = await userService.profile(user.login);

      dispatch(
        createAction("updateState")({
          loadingUserProfile: false,
          loadingUserId: 0,
          userProfile: data,
          modalVisible: true,
        })
      );
    } catch (e) {
      addToast(handleApiError(e), {
        appearance: "error",
        autoDismiss: true,
      });

      dispatch(
        createAction("updateState")({
          loadingUserProfile: false,
        })
      );

      throw e;
    }
  };

  return {
    execute: useCallback(execute, [addToast, dispatch]),
  };
};
