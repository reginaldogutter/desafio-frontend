import React, { useContext } from "react";
import Pagination from "react-js-pagination";

import { store } from "../../store";
import { createAction } from "../../utils/utils";
import { useSearchUsers, useUserProfile } from "../../models/users";

import Layout from "../../layout";
import Card from "../../components/card";
import Modal from "../../components/modal";

import "./index.css";

export default function Results() {
  const {
    dispatch,
    state: {
      searchTerm,
      modalVisible,
      results,
      loadingUserProfile,
      loadingUserId,
      userProfile,
      searchPage,
      searchPerPage,
      totalItems,
    },
  } = useContext(store);

  const { execute: executeSearch } = useSearchUsers();
  const { execute } = useUserProfile();

  const doPagination = async (pageNumber) => {
    await executeSearch(searchTerm, pageNumber, searchPerPage);
  };

  return (
    <Layout hasImageBackground>
      <div className="results-container">
        <div className="results-title">
          <p>{`Resultados para: ${searchTerm}`}</p>
        </div>
        {results.length > 0 && (
          <>
            <ul className="grid-container">
              {results.map((result) => (
                <li className="grid-item" key={`${result.id}`}>
                  <Card
                    data={result}
                    loading={loadingUserProfile}
                    userId={loadingUserId}
                    onSeeDetails={async () => execute(result)}
                  />
                </li>
              ))}
            </ul>
            <div className="grid-pagination">
              <Pagination
                activePage={searchPage}
                itemsCountPerPage={searchPerPage}
                totalItemsCount={totalItems}
                pageRangeDisplayed={5}
                onChange={doPagination}
              />
            </div>
          </>
        )}
        {results.length === 0 && (
          <p className="no-results-found">Nenhum resultado encontrado!</p>
        )}
      </div>
      <Modal
        visible={modalVisible}
        data={userProfile}
        onClose={() => {
          dispatch(
            createAction("updateState")({
              modalVisible: false,
              userProfile: null,
            })
          );
        }}
      />
    </Layout>
  );
}
