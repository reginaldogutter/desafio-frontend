import React from "react";
import Layout from "../../layout";

import logoShadow from "../../assets/logotype-shadow.svg";
import "./index.css";

export default function Home() {
  return (
    <Layout>
      <div className="home-container">
        <img
          src={logoShadow}
          width="100%"
          alt="The Git Search - Buscas no Github"
        />
      </div>
    </Layout>
  );
}
