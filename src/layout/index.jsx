import React from "react";
import { format } from "date-fns";

import Header from "../components/header";
import Footer from "../components/footer";

import "./index.css";

export default function Layout({ children, hasImageBackground }) {
  return (
    <div className={`layout${hasImageBackground ? " image-bg" : ""}`}>
      <Header />
      {children}
      <Footer
        copyright={`Projetado por: Reginaldo Gutter - ${format(
          new Date(),
          "dd/MM/yyyy"
        )}`}
      />
    </div>
  );
}
