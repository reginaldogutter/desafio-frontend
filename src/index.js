import React from "react";
import ReactDOM from "react-dom";
import { ToastProvider } from "react-toast-notifications";

import Router from "./router";
import { StateProvider } from "./store";

import "./global.css";

const app = (
  <StateProvider>
    <ToastProvider>
      <Router />
    </ToastProvider>
  </StateProvider>
);

ReactDOM.render(app, document.getElementById("root"));
